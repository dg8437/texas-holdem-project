﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    partial class TexasHoldEm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Generate Base Values for the Players Banks

        private int Player1Money = 500;
        private int Player2Money = 500;
        private int Player3Money = 500;
        private int Player4Money = 500;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TexasHoldEm));
            this.picPlayer1 = new System.Windows.Forms.PictureBox();
            this.picPlayer2 = new System.Windows.Forms.PictureBox();
            this.picAi6 = new System.Windows.Forms.PictureBox();
            this.picAi5 = new System.Windows.Forms.PictureBox();
            this.picAi2 = new System.Windows.Forms.PictureBox();
            this.picAi1 = new System.Windows.Forms.PictureBox();
            this.picAi4 = new System.Windows.Forms.PictureBox();
            this.picAi3 = new System.Windows.Forms.PictureBox();
            this.picRiver3 = new System.Windows.Forms.PictureBox();
            this.picRiver1 = new System.Windows.Forms.PictureBox();
            this.picRiver2 = new System.Windows.Forms.PictureBox();
            this.picRiver4 = new System.Windows.Forms.PictureBox();
            this.picRiver5 = new System.Windows.Forms.PictureBox();
            this.btnCall = new System.Windows.Forms.Button();
            this.btnFold = new System.Windows.Forms.Button();
            this.btnRaise = new System.Windows.Forms.Button();
            this.lblmoneydesc = new System.Windows.Forms.Label();
            this.lblPot = new System.Windows.Forms.Label();
            this.btnRules = new System.Windows.Forms.Button();
            this.lblPlyr1Bnk = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblPlyr2Bnk = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblPlyr3Bnk = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPlyr4Bnk = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblP3 = new System.Windows.Forms.Label();
            this.lblP4 = new System.Windows.Forms.Label();
            this.lblP1 = new System.Windows.Forms.Label();
            this.lblP2 = new System.Windows.Forms.Label();
            this.btnShuffle = new System.Windows.Forms.Button();
            this.lblPlyr1Bet = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCheck = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPlyr2Bet = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblPlyr4Bet = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblPlyr3Bet = new System.Windows.Forms.Label();
            this.YouLosePB = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picPlayer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPlayer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YouLosePB)).BeginInit();
            this.SuspendLayout();
            // 
            // picPlayer1
            // 
            this.picPlayer1.BackColor = System.Drawing.SystemColors.Highlight;
            this.picPlayer1.Image = ((System.Drawing.Image)(resources.GetObject("picPlayer1.Image")));
            this.picPlayer1.Location = new System.Drawing.Point(369, 370);
            this.picPlayer1.Name = "picPlayer1";
            this.picPlayer1.Size = new System.Drawing.Size(68, 97);
            this.picPlayer1.TabIndex = 0;
            this.picPlayer1.TabStop = false;
            // 
            // picPlayer2
            // 
            this.picPlayer2.BackColor = System.Drawing.SystemColors.Highlight;
            this.picPlayer2.Image = ((System.Drawing.Image)(resources.GetObject("picPlayer2.Image")));
            this.picPlayer2.Location = new System.Drawing.Point(397, 370);
            this.picPlayer2.Name = "picPlayer2";
            this.picPlayer2.Size = new System.Drawing.Size(68, 97);
            this.picPlayer2.TabIndex = 1;
            this.picPlayer2.TabStop = false;
            // 
            // picAi6
            // 
            this.picAi6.BackColor = System.Drawing.SystemColors.Highlight;
            this.picAi6.Image = ((System.Drawing.Image)(resources.GetObject("picAi6.Image")));
            this.picAi6.Location = new System.Drawing.Point(765, 186);
            this.picAi6.Name = "picAi6";
            this.picAi6.Size = new System.Drawing.Size(68, 97);
            this.picAi6.TabIndex = 3;
            this.picAi6.TabStop = false;
            // 
            // picAi5
            // 
            this.picAi5.BackColor = System.Drawing.SystemColors.Highlight;
            this.picAi5.Image = ((System.Drawing.Image)(resources.GetObject("picAi5.Image")));
            this.picAi5.Location = new System.Drawing.Point(737, 186);
            this.picAi5.Name = "picAi5";
            this.picAi5.Size = new System.Drawing.Size(68, 97);
            this.picAi5.TabIndex = 2;
            this.picAi5.TabStop = false;
            // 
            // picAi2
            // 
            this.picAi2.BackColor = System.Drawing.SystemColors.Highlight;
            this.picAi2.Image = ((System.Drawing.Image)(resources.GetObject("picAi2.Image")));
            this.picAi2.Location = new System.Drawing.Point(54, 186);
            this.picAi2.Name = "picAi2";
            this.picAi2.Size = new System.Drawing.Size(68, 97);
            this.picAi2.TabIndex = 5;
            this.picAi2.TabStop = false;
            // 
            // picAi1
            // 
            this.picAi1.BackColor = System.Drawing.SystemColors.Highlight;
            this.picAi1.Image = ((System.Drawing.Image)(resources.GetObject("picAi1.Image")));
            this.picAi1.Location = new System.Drawing.Point(26, 186);
            this.picAi1.Name = "picAi1";
            this.picAi1.Size = new System.Drawing.Size(68, 97);
            this.picAi1.TabIndex = 4;
            this.picAi1.TabStop = false;
            // 
            // picAi4
            // 
            this.picAi4.BackColor = System.Drawing.SystemColors.Highlight;
            this.picAi4.Image = ((System.Drawing.Image)(resources.GetObject("picAi4.Image")));
            this.picAi4.Location = new System.Drawing.Point(397, 38);
            this.picAi4.Name = "picAi4";
            this.picAi4.Size = new System.Drawing.Size(68, 97);
            this.picAi4.TabIndex = 7;
            this.picAi4.TabStop = false;
            // 
            // picAi3
            // 
            this.picAi3.BackColor = System.Drawing.SystemColors.Highlight;
            this.picAi3.Image = ((System.Drawing.Image)(resources.GetObject("picAi3.Image")));
            this.picAi3.Location = new System.Drawing.Point(369, 38);
            this.picAi3.Name = "picAi3";
            this.picAi3.Size = new System.Drawing.Size(68, 97);
            this.picAi3.TabIndex = 6;
            this.picAi3.TabStop = false;
            // 
            // picRiver3
            // 
            this.picRiver3.BackColor = System.Drawing.SystemColors.Highlight;
            this.picRiver3.Image = ((System.Drawing.Image)(resources.GetObject("picRiver3.Image")));
            this.picRiver3.Location = new System.Drawing.Point(384, 186);
            this.picRiver3.Name = "picRiver3";
            this.picRiver3.Size = new System.Drawing.Size(68, 97);
            this.picRiver3.TabIndex = 8;
            this.picRiver3.TabStop = false;
            // 
            // picRiver1
            // 
            this.picRiver1.BackColor = System.Drawing.SystemColors.Highlight;
            this.picRiver1.Image = ((System.Drawing.Image)(resources.GetObject("picRiver1.Image")));
            this.picRiver1.Location = new System.Drawing.Point(236, 186);
            this.picRiver1.Name = "picRiver1";
            this.picRiver1.Size = new System.Drawing.Size(68, 97);
            this.picRiver1.TabIndex = 8;
            this.picRiver1.TabStop = false;
            // 
            // picRiver2
            // 
            this.picRiver2.BackColor = System.Drawing.SystemColors.Highlight;
            this.picRiver2.Image = ((System.Drawing.Image)(resources.GetObject("picRiver2.Image")));
            this.picRiver2.Location = new System.Drawing.Point(310, 186);
            this.picRiver2.Name = "picRiver2";
            this.picRiver2.Size = new System.Drawing.Size(68, 97);
            this.picRiver2.TabIndex = 9;
            this.picRiver2.TabStop = false;
            // 
            // picRiver4
            // 
            this.picRiver4.BackColor = System.Drawing.SystemColors.Highlight;
            this.picRiver4.Image = ((System.Drawing.Image)(resources.GetObject("picRiver4.Image")));
            this.picRiver4.Location = new System.Drawing.Point(458, 186);
            this.picRiver4.Name = "picRiver4";
            this.picRiver4.Size = new System.Drawing.Size(68, 97);
            this.picRiver4.TabIndex = 10;
            this.picRiver4.TabStop = false;
            // 
            // picRiver5
            // 
            this.picRiver5.BackColor = System.Drawing.SystemColors.Highlight;
            this.picRiver5.Image = ((System.Drawing.Image)(resources.GetObject("picRiver5.Image")));
            this.picRiver5.Location = new System.Drawing.Point(532, 186);
            this.picRiver5.Name = "picRiver5";
            this.picRiver5.Size = new System.Drawing.Size(68, 97);
            this.picRiver5.TabIndex = 11;
            this.picRiver5.TabStop = false;
            // 
            // btnCall
            // 
            this.btnCall.Location = new System.Drawing.Point(26, 370);
            this.btnCall.Name = "btnCall";
            this.btnCall.Size = new System.Drawing.Size(75, 23);
            this.btnCall.TabIndex = 24;
            this.btnCall.Text = "Call";
            this.btnCall.UseVisualStyleBackColor = true;
            this.btnCall.Click += new System.EventHandler(this.btnCall_Click);
            // 
            // btnFold
            // 
            this.btnFold.Location = new System.Drawing.Point(107, 370);
            this.btnFold.Name = "btnFold";
            this.btnFold.Size = new System.Drawing.Size(75, 23);
            this.btnFold.TabIndex = 25;
            this.btnFold.Text = "Fold";
            this.btnFold.UseVisualStyleBackColor = true;
            this.btnFold.Click += new System.EventHandler(this.btnFold_Click);
            // 
            // btnRaise
            // 
            this.btnRaise.Location = new System.Drawing.Point(188, 370);
            this.btnRaise.Name = "btnRaise";
            this.btnRaise.Size = new System.Drawing.Size(75, 23);
            this.btnRaise.TabIndex = 26;
            this.btnRaise.Text = "Raise";
            this.btnRaise.UseVisualStyleBackColor = true;
            this.btnRaise.Click += new System.EventHandler(this.btnRaise_Click);
            // 
            // lblmoneydesc
            // 
            this.lblmoneydesc.AutoSize = true;
            this.lblmoneydesc.Location = new System.Drawing.Point(333, 299);
            this.lblmoneydesc.Name = "lblmoneydesc";
            this.lblmoneydesc.Size = new System.Drawing.Size(26, 13);
            this.lblmoneydesc.TabIndex = 27;
            this.lblmoneydesc.Text = "Pot:";
            // 
            // lblPot
            // 
            this.lblPot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPot.Location = new System.Drawing.Point(365, 298);
            this.lblPot.Name = "lblPot";
            this.lblPot.Size = new System.Drawing.Size(100, 23);
            this.lblPot.TabIndex = 28;
            // 
            // btnRules
            // 
            this.btnRules.Location = new System.Drawing.Point(769, 370);
            this.btnRules.Name = "btnRules";
            this.btnRules.Size = new System.Drawing.Size(96, 23);
            this.btnRules.TabIndex = 29;
            this.btnRules.Text = "How To Play";
            this.btnRules.UseVisualStyleBackColor = true;
            this.btnRules.Click += new System.EventHandler(this.btnRules_Click);
            // 
            // lblPlyr1Bnk
            // 
            this.lblPlyr1Bnk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr1Bnk.Location = new System.Drawing.Point(369, 149);
            this.lblPlyr1Bnk.Name = "lblPlyr1Bnk";
            this.lblPlyr1Bnk.Size = new System.Drawing.Size(100, 23);
            this.lblPlyr1Bnk.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(366, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Player Bank:";
            // 
            // lblPlyr2Bnk
            // 
            this.lblPlyr2Bnk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr2Bnk.Location = new System.Drawing.Point(733, 299);
            this.lblPlyr2Bnk.Name = "lblPlyr2Bnk";
            this.lblPlyr2Bnk.Size = new System.Drawing.Size(100, 23);
            this.lblPlyr2Bnk.TabIndex = 39;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(730, 286);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Player Bank:";
            // 
            // lblPlyr3Bnk
            // 
            this.lblPlyr3Bnk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr3Bnk.Location = new System.Drawing.Point(369, 479);
            this.lblPlyr3Bnk.Name = "lblPlyr3Bnk";
            this.lblPlyr3Bnk.Size = new System.Drawing.Size(100, 23);
            this.lblPlyr3Bnk.TabIndex = 41;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(366, 466);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "Player Bank:";
            // 
            // lblPlyr4Bnk
            // 
            this.lblPlyr4Bnk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr4Bnk.Location = new System.Drawing.Point(26, 298);
            this.lblPlyr4Bnk.Name = "lblPlyr4Bnk";
            this.lblPlyr4Bnk.Size = new System.Drawing.Size(100, 23);
            this.lblPlyr4Bnk.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 285);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Player Bank:";
            // 
            // lblP3
            // 
            this.lblP3.BackColor = System.Drawing.Color.Green;
            this.lblP3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP3.ForeColor = System.Drawing.Color.White;
            this.lblP3.Location = new System.Drawing.Point(369, 345);
            this.lblP3.Name = "lblP3";
            this.lblP3.Size = new System.Drawing.Size(96, 18);
            this.lblP3.TabIndex = 44;
            this.lblP3.Visible = false;
            // 
            // lblP4
            // 
            this.lblP4.BackColor = System.Drawing.Color.Green;
            this.lblP4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP4.ForeColor = System.Drawing.Color.White;
            this.lblP4.Location = new System.Drawing.Point(25, 162);
            this.lblP4.Name = "lblP4";
            this.lblP4.Size = new System.Drawing.Size(96, 18);
            this.lblP4.TabIndex = 45;
            this.lblP4.Visible = false;
            // 
            // lblP1
            // 
            this.lblP1.BackColor = System.Drawing.Color.Green;
            this.lblP1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP1.ForeColor = System.Drawing.Color.White;
            this.lblP1.Location = new System.Drawing.Point(369, 17);
            this.lblP1.Name = "lblP1";
            this.lblP1.Size = new System.Drawing.Size(96, 18);
            this.lblP1.TabIndex = 46;
            this.lblP1.Visible = false;
            // 
            // lblP2
            // 
            this.lblP2.BackColor = System.Drawing.Color.Green;
            this.lblP2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP2.ForeColor = System.Drawing.Color.White;
            this.lblP2.Location = new System.Drawing.Point(737, 162);
            this.lblP2.Name = "lblP2";
            this.lblP2.Size = new System.Drawing.Size(96, 18);
            this.lblP2.TabIndex = 47;
            this.lblP2.Visible = false;
            // 
            // btnShuffle
            // 
            this.btnShuffle.Enabled = false;
            this.btnShuffle.Location = new System.Drawing.Point(688, 370);
            this.btnShuffle.Name = "btnShuffle";
            this.btnShuffle.Size = new System.Drawing.Size(75, 23);
            this.btnShuffle.TabIndex = 48;
            this.btnShuffle.Text = "Next Hand";
            this.btnShuffle.UseVisualStyleBackColor = true;
            this.btnShuffle.Click += new System.EventHandler(this.btnShuffle_Click);
            // 
            // lblPlyr1Bet
            // 
            this.lblPlyr1Bet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr1Bet.Location = new System.Drawing.Point(471, 112);
            this.lblPlyr1Bet.Name = "lblPlyr1Bet";
            this.lblPlyr1Bet.Size = new System.Drawing.Size(68, 23);
            this.lblPlyr1Bet.TabIndex = 49;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(468, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 50;
            this.label3.Text = "Bet:";
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(269, 370);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(75, 23);
            this.btnCheck.TabIndex = 51;
            this.btnCheck.Text = "Check";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(660, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 53;
            this.label5.Text = "Bet:";
            // 
            // lblPlyr2Bet
            // 
            this.lblPlyr2Bet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr2Bet.Location = new System.Drawing.Point(663, 260);
            this.lblPlyr2Bet.Name = "lblPlyr2Bet";
            this.lblPlyr2Bet.Size = new System.Drawing.Size(68, 23);
            this.lblPlyr2Bet.TabIndex = 52;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(125, 247);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 55;
            this.label9.Text = "Bet:";
            // 
            // lblPlyr4Bet
            // 
            this.lblPlyr4Bet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr4Bet.Location = new System.Drawing.Point(128, 260);
            this.lblPlyr4Bet.Name = "lblPlyr4Bet";
            this.lblPlyr4Bet.Size = new System.Drawing.Size(68, 23);
            this.lblPlyr4Bet.TabIndex = 54;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(468, 431);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 13);
            this.label11.TabIndex = 57;
            this.label11.Text = "Bet:";
            // 
            // lblPlyr3Bet
            // 
            this.lblPlyr3Bet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPlyr3Bet.Location = new System.Drawing.Point(471, 444);
            this.lblPlyr3Bet.Name = "lblPlyr3Bet";
            this.lblPlyr3Bet.Size = new System.Drawing.Size(68, 23);
            this.lblPlyr3Bet.TabIndex = 56;
            // 
            // YouLosePB
            // 
            this.YouLosePB.Location = new System.Drawing.Point(321, 162);
            this.YouLosePB.Name = "YouLosePB";
            this.YouLosePB.Size = new System.Drawing.Size(190, 121);
            this.YouLosePB.TabIndex = 58;
            this.YouLosePB.TabStop = false;
            this.YouLosePB.Visible = false;
            // 
            // TexasHoldEm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 513);
            this.Controls.Add(this.YouLosePB);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblPlyr3Bet);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblPlyr4Bet);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblPlyr2Bet);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPlyr1Bet);
            this.Controls.Add(this.btnShuffle);
            this.Controls.Add(this.lblP2);
            this.Controls.Add(this.lblP1);
            this.Controls.Add(this.lblP4);
            this.Controls.Add(this.lblP3);
            this.Controls.Add(this.lblPlyr4Bnk);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblPlyr3Bnk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPlyr2Bnk);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblPlyr1Bnk);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnRules);
            this.Controls.Add(this.lblPot);
            this.Controls.Add(this.lblmoneydesc);
            this.Controls.Add(this.btnRaise);
            this.Controls.Add(this.btnFold);
            this.Controls.Add(this.btnCall);
            this.Controls.Add(this.picRiver5);
            this.Controls.Add(this.picRiver4);
            this.Controls.Add(this.picRiver2);
            this.Controls.Add(this.picRiver1);
            this.Controls.Add(this.picRiver3);
            this.Controls.Add(this.picAi4);
            this.Controls.Add(this.picAi3);
            this.Controls.Add(this.picAi2);
            this.Controls.Add(this.picAi1);
            this.Controls.Add(this.picAi6);
            this.Controls.Add(this.picAi5);
            this.Controls.Add(this.picPlayer2);
            this.Controls.Add(this.picPlayer1);
            this.Name = "TexasHoldEm";
            this.Text = "TexasHoldE\'m";
            this.Load += new System.EventHandler(this.TexasHoldEm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picPlayer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPlayer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAi3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRiver5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YouLosePB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion


        private System.Windows.Forms.PictureBox picPlayer1;
        private System.Windows.Forms.PictureBox picPlayer2;
        private System.Windows.Forms.PictureBox picAi6;
        private System.Windows.Forms.PictureBox picAi5;
        private System.Windows.Forms.PictureBox picAi2;
        private System.Windows.Forms.PictureBox picAi1;
        private System.Windows.Forms.PictureBox picAi4;
        private System.Windows.Forms.PictureBox picAi3;
        private System.Windows.Forms.PictureBox picRiver3;
        private System.Windows.Forms.PictureBox picRiver1;
        private System.Windows.Forms.PictureBox picRiver2;
        private System.Windows.Forms.PictureBox picRiver4;
        private System.Windows.Forms.PictureBox picRiver5;
        private System.Windows.Forms.Button btnCall;
        private System.Windows.Forms.Button btnFold;
        private System.Windows.Forms.Button btnRaise;
        private System.Windows.Forms.Label lblmoneydesc;
        private System.Windows.Forms.Label lblPot;
        private System.Windows.Forms.Button btnRules;
        private System.Windows.Forms.Label lblPlyr1Bnk;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblPlyr2Bnk;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPlyr3Bnk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPlyr4Bnk;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblP3;
        public System.Windows.Forms.Label lblP4;
        public System.Windows.Forms.Label lblP1;
        public System.Windows.Forms.Label lblP2;
        private System.Windows.Forms.Button btnShuffle;
        private System.Windows.Forms.Label lblPlyr1Bet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPlyr2Bet;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblPlyr4Bet;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblPlyr3Bet;
        private PictureBox YouLosePB;
    }
}
