﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class CalcBank
    {
        public List<decimal> PlayerBanks = new List<decimal>();
        public List<decimal> SubtractionAmounts = new List<decimal>();
        public decimal WinningPlayerBank;
        public decimal PotAmmount;

        public CalcBank(List<decimal> Banks)
        {
            PlayerBanks = Banks;
        }

        public CalcBank(List<decimal> Banks, List<decimal> Bets)
        {
            PlayerBanks = Banks;
            SubtractionAmounts = Bets;
        }

        public CalcBank(decimal HandWinner, decimal Pot)
        {
            WinningPlayerBank = HandWinner;
            PotAmmount = Pot;
        }
        //Calculated players banks rolls minus the their bet amount
        public void NewBankRoll()
        {
            for (int i = 0; i < PlayerBanks.Count; i++)
            {
                PlayerBanks[i] = PlayerBanks[i] - SubtractionAmounts[i];
            }
        }
        //Place holder method for round winners
        public void WinnerWinnerChickenDinner()
        {
            WinningPlayerBank += PotAmmount;
        }
    }
}

