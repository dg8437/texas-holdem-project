﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{
    public class Token
    {
        public List<string> rotatingTokens = new List<string>();


        public Token (List<string> startingTokens)
        {
            rotatingTokens = startingTokens;
        }

       
        public void tokenRotate()
        {
            string temp = rotatingTokens[rotatingTokens.Count - 1];
            for(var i = rotatingTokens.Count - 1; i > 0; i--)
            {
                rotatingTokens[i] = rotatingTokens[i - 1];
            }
            rotatingTokens[0] = temp;
        }
       
    }
}
