﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public class CalcBet
    {
        public List<decimal> PlayerBets = new List<decimal>();

        public CalcBet(List<decimal> Bets)
        {
            PlayerBets = Bets;
        }

        //Rotates intial betting amounts following the rotation of token positions
        public void BetRotate()
        {
            decimal temp = PlayerBets[PlayerBets.Count - 1];
            for (var i = PlayerBets.Count - 1; i > 0; i--)
            {
                PlayerBets[i] = PlayerBets[i - 1];
            }
            PlayerBets[0] = temp;
        }
        //Calls the highest bet at the table
        public void Call()
        {
            var max = PlayerBets.Max();
            PlayerBets[2] = max;
        }
        //Current raise functionality
        public void Raise()
        {
            PlayerBets[2] += 50;
        }
        //Method to calculating the pot total. 
        public decimal BetSum()
        {
            decimal total = PlayerBets.Sum();

            return total;
        }


    }
}