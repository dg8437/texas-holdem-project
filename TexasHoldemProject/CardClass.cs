﻿using System;
using System.Collections.Generic;

namespace WindowsFormsApp3
{

    public class Card
    {
        private int value;
        private int suit;
        private string name;
        private string image;
        private string faceval;

        public Card()
        {
            Calculate();
            CardName();
            Imager();
        }
        /// <summary>
        /// The following code comment is a section of code sourced from a git project 
        /// that can be found here https://github.com/platatat/SnapCall
        /// Weve attempted to implement some of what can be found there but it is still 
        /// very much a work in progress. All code from said repository will have this 
        /// warning before hand.
        /// </summary>
        /*
        public static ConsoleColor[] SuitColors = { ConsoleColor.Green, ConsoleColor.Red, ConsoleColor.Blue, ConsoleColor.Green };

        public SnapCall.Enums.Rank Rank { get; set; }
        public SnapCall.Enums.Suit Suit { get; set; }

        private static int[] rankPrimes = new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41 };
        private static int[] suitPrimes = new int[] { 43, 47, 53, 59 };

        public int PrimeRank { get { return rankPrimes[(int)Rank]; } }
        public int PrimeSuit { get { return suitPrimes[(int)Suit]; } }

        public bool Equals(Card other)
        {
            return this.Rank == other.Rank && this.Suit == other.Suit;
        }

        public int GetHashCode(Card c)
        {
            return c.PrimeRank * c.PrimeSuit;
        }

        public Card(string s)
        {
            var chars = s.ToUpper().ToCharArray();
            if (chars.Length != 2) throw new ArgumentException("Card string must be length 2");
            switch (chars[0])
            {
                case '2': this.Rank = SnapCall.Enums.Rank.Two; break;
                case '3': this.Rank = SnapCall.Enums.Rank.Three; break;
                case '4': this.Rank = SnapCall.Enums.Rank.Four; break;
                case '5': this.Rank = SnapCall.Enums.Rank.Five; break;
                case '6': this.Rank = SnapCall.Enums.Rank.Six; break;
                case '7': this.Rank = SnapCall.Enums.Rank.Seven; break;
                case '8': this.Rank = SnapCall.Enums.Rank.Eight; break;
                case '9': this.Rank = SnapCall.Enums.Rank.Nine; break;
                case 'T': this.Rank = SnapCall.Enums.Rank.Ten; break;
                case 'J': this.Rank = SnapCall.Enums.Rank.Jack; break;
                case 'Q': this.Rank = SnapCall.Enums.Rank.Queen; break;
                case 'K': this.Rank = SnapCall.Enums.Rank.King; break;
                case 'A': this.Rank = SnapCall.Enums.Rank.Ace; break;
                default: throw new ArgumentException("Card string rank not valid");
            }
            switch (chars[1])
            {
                case 'S': this.Suit = SnapCall.Enums.Suit.Spades; break;
                case 'H': this.Suit = SnapCall.Enums.Suit.Hearts; break;
                case 'D': this.Suit = SnapCall.Enums.Suit.Diamonds; break;
                case 'C': this.Suit = SnapCall.Enums.Suit.Clubs; break;
                default: throw new ArgumentException("Card string suit not valid");
            }
        }

        public override string ToString()
        {
            char[] ranks = "23456789TJQKA".ToCharArray();
            char[] suits = { '♠', '♥', '♦', '♣' };

            return ranks[(int)Rank].ToString() + suits[(int)Suit].ToString();
        }
        */
        public void Calculate()
        {
            // import random number generator
            Random rand = new Random();

            //constants
            const int max = 52;
            const int min = 1;
            const int CardTypes = 13;

            //generate a random number in the deck
            numberGenerator:
            int randNum = rand.Next(min, max);

            //use that random number to generate a value and suit for the card
            int suit = randNum / CardTypes;
            int value = randNum % CardTypes;

            //Track recent cards so no duplicates are created
            Tuple<int, int> tmp = new Tuple<int, int>(suit, value);
            Dictionary<string, Tuple<int, int>> cardDictionary = CardStore.GetCardDictionary();
            if (cardDictionary.ContainsValue(tmp))
            {
                goto numberGenerator;
            }

            //If a new unique card is created store the values
            this.suit = suit;
            this.value = value;

        }

        private void CardName()
        {
            string name = "";

            //Give this card a meaningful name using the value and suit numbers
            switch (this.value)
            {
                case 0: name += "Ace of "; break;
                case 1: name += "2 of "; break;
                case 2: name += "3 of "; break;
                case 3: name += "4 of "; break;
                case 4: name += "5 of "; break;
                case 5: name += "6 of "; break;
                case 6: name += "7 of "; break;
                case 7: name += "8 of "; break;
                case 8: name += "9 of "; break;
                case 9: name += "10 of "; break;
                case 10: name += "Jack of "; break;
                case 11: name += "Queen of "; break;
                case 12: name += "King of "; break;
            }
            faceval = name;
            switch (this.suit)
            {
                case 0: name += "Clubs"; break;
                case 1: name += "Diamonds"; break;
                case 2: name += "Hearts"; break;
                case 3: name += "Spades"; break;
            }

            // If the length is null for some reason set the value to Joker as a joke (maybe we ran out of cards?)
            if (name.Length != 0)
            {
                this.name = name;
            }
            else
            {
                this.name = "Joker";
            }

        }

        private void Imager()
        {
            string image = "";
            image = this.name;

            image = image.Replace(" ", "_");
            image = image.ToLower();
            image = @"..\..\card-BMPs\" + image + ".bmp";

            this.image = image;
        }

        public string GetImage()
        {
            return this.image;
        }
        public string Getfaceval()
        {
            return this.faceval;
        }
        // return this card's name
        public string GetName()
        {
            return this.name;
        }

        // return this card's suit number
        public int GetSuit()
        {
            return this.suit;
        }

        // return this card's value
        public int GetValue()
        {
            return this.value;
        }
    }
}
