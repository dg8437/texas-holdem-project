﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp3
{

    public class Hand
    {
        private int score;
        private List<string> cardList = new List<string>();

        public Hand()
        {
            this.score = 0;
        }

        public int Score
        {
            get
            {
                return this.score;
            }
            set
            {
                this.score = value;
            }
        }

        public List<string> CardList
        {
            get
            {
                return this.cardList;
            }
            set
            {
                this.cardList = value;
            }
        }


        public void DrawCard(CardStore deck)
        {
            Card card = deck.DrawCard();
            cardList.Add(card.GetImage());


        }

        public void reset()
        {
            cardList.Clear();

        }

    }
}
    
