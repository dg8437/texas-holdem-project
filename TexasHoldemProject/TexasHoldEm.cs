﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace WindowsFormsApp3
{
    public partial class TexasHoldEm : Form
    {
        List<string> startingTokens = new List<string>() { "DEALER", "SMALL BLIND", "BIG BLIND", "" };
        List<decimal> PlayerBanks = new List<decimal>() { 1000, 1000, 1000, 1000 };
        List<decimal> PlayerBets = new List<decimal>() { 100, 25, 50, 0 };
        CardStore deck = new CardStore();
        Hand player = new Hand();
        Hand river = new Hand();
        int Betcount = 0;
        SoundPlayer jukebox = new SoundPlayer();
        int RoundCount = 0;

        public TexasHoldEm()
        {
            InitializeComponent();
            jukebox.SoundLocation=@"../../Resources/backgroundMusic.wav";
            jukebox.PlayLooping();

        }
        private void TexasHoldEm_Load(object sender, EventArgs e)
        {
            //output initial player bank
            lblPlyr1Bnk.Text = PlayerBanks[0].ToString("c");
            lblPlyr2Bnk.Text = PlayerBanks[1].ToString("c");
            lblPlyr3Bnk.Text = PlayerBanks[2].ToString("c");
            lblPlyr4Bnk.Text = PlayerBanks[3].ToString("c");

            //output initial betting amount
            lblPlyr1Bet.Text = PlayerBets[0].ToString("c");
            lblPlyr2Bet.Text = PlayerBets[1].ToString("c");
            lblPlyr3Bet.Text = PlayerBets[2].ToString("c");
            lblPlyr4Bet.Text = PlayerBets[3].ToString("c");

            //output initial pot amount
            var Bets = new CalcBet(PlayerBets);
            lblPot.Text = Bets.BetSum().ToString("c");

            //initialize token positions
            lblP1.Text = startingTokens[0];
            lblP2.Text = startingTokens[1];
            lblP3.Text = startingTokens[2];
            lblP4.Text = startingTokens[3];


            //display tokens
            var lblTknCollection = new[] { lblP1, lblP2, lblP3, lblP4 };
            foreach (Control lblT in lblTknCollection)
            {
                if (lblT.Text != "")
                {
                    lblT.Visible = true;

                }
                else
                {
                    lblT.Visible = false;
                }
            }

            NewRound();

        }
        private void NewRound()
        {
            deck.reset();
            player.reset();
            river.reset();
            
            PictureBox[] Pboxes = { picPlayer1, picPlayer2 };
            PictureBox[] Rboxes = { picRiver1, picRiver2, picRiver3, picRiver4, picRiver5 };
           
            if (PlayerBanks[2]<=0 && Betcount>1 )
            {
                YouLosePB.Visible = true;
                using (var soundPlayer = new SoundPlayer(@"../../Resources/dark-souls-you-died-sound-effect.wav"))
                {
                    soundPlayer.Play(); // can also use soundPlayer.PlaySync()
                    MessageBox.Show("Game Over");
                }
                System.Environment.Exit(0);
            }
            else if (PlayerBanks[0] <=0 || PlayerBanks[1] <= 0 || PlayerBanks[3] <= 0)
            {
                MessageBox.Show("You Win");
                System.Environment.Exit(0);
            }
            Betcount = 0;
            for (int count = 0; count < 5; count++)
            {
                Rboxes[count].ImageLocation = @"../../card-BMPs/b2fv.bmp";
                Rboxes[count].Refresh();
                Rboxes[count].BringToFront();
            }
            for (int count = 0; count < 2; count++)
            {
                player.DrawCard(deck);
                Pboxes[count].ImageLocation = player.CardList[count];
                Pboxes[count].Refresh();
                Pboxes[count].BringToFront();

            }
            btnCall.Enabled = true;
            btnFold.Enabled = true;
            btnRaise.Enabled = true;
            btnCheck.Enabled = true;
            if (RoundCount > 0)
            {
                var tokenIndicator = new Token(startingTokens);
                tokenIndicator.tokenRotate();
                lblP1.Text = tokenIndicator.rotatingTokens[0];
                lblP2.Text = tokenIndicator.rotatingTokens[1];
                lblP3.Text = tokenIndicator.rotatingTokens[2];
                lblP4.Text = tokenIndicator.rotatingTokens[3];

                var lblTknCollection = new[] { lblP1, lblP2, lblP3, lblP4 };
                foreach (Control lbl in lblTknCollection)
                {
                    if (lbl.Text != "")
                    {
                        lbl.Visible = true;
                    }
                    else
                    {
                        lbl.Visible = false;
                    }
                }
                var Bets = new CalcBet(PlayerBets);
                var Banks = new CalcBank(PlayerBanks, Bets.PlayerBets);
                Banks.NewBankRoll();
                lblPlyr1Bnk.Text = Banks.PlayerBanks[0].ToString("c");
                lblPlyr2Bnk.Text = Banks.PlayerBanks[1].ToString("c");
                lblPlyr3Bnk.Text = Banks.PlayerBanks[2].ToString("c");
                lblPlyr4Bnk.Text = Banks.PlayerBanks[3].ToString("c");

                Bets.BetRotate();
                lblPlyr1Bet.Text = Bets.PlayerBets[0].ToString("c");
                lblPlyr2Bet.Text = Bets.PlayerBets[1].ToString("c");
                lblPlyr3Bet.Text = Bets.PlayerBets[2].ToString("c");
                lblPlyr4Bet.Text = Bets.PlayerBets[3].ToString("c");
                lblPot.Text = Bets.BetSum().ToString("c");

            }

            RoundCount++;
        }
        private void Flop()
        {
            PictureBox[] Rboxes = { picRiver1, picRiver2, picRiver3 };
            for (int count = 0; count < 3; count++)
            {
                river.DrawCard(deck);
                Rboxes[count].ImageLocation = river.CardList[count];
                Rboxes[count].Refresh();
                Rboxes[count].BringToFront();
            }
        }
        private void Turn()
        {
            PictureBox[] Rboxes = { picRiver4 };

            river.DrawCard(deck);
            Rboxes[0].ImageLocation = river.CardList[3];
            Rboxes[0].Refresh();
            Rboxes[0].BringToFront();

        }
        private void River()
        {
            PictureBox[] Rboxes = { picRiver5 };

            river.DrawCard(deck);
            Rboxes[0].ImageLocation = river.CardList[4];
            Rboxes[0].Refresh();
            Rboxes[0].BringToFront();
        }


        private void btnCall_Click(object sender, EventArgs e)
        {
            //Logic is not yet built to handle betting rounds before the flop/turn/river. Clicking Call moves the round to the next card(s) reveal.
            var Bets = new CalcBet(PlayerBets);
            Bets.Call();
            lblPlyr1Bet.Text = Bets.PlayerBets[0].ToString("c");
            lblPlyr2Bet.Text = Bets.PlayerBets[1].ToString("c");
            lblPlyr3Bet.Text = Bets.PlayerBets[2].ToString("c");
            lblPlyr4Bet.Text = Bets.PlayerBets[3].ToString("c");

            var Banks = new CalcBank(PlayerBanks, PlayerBets);
            Banks.NewBankRoll();
            lblPlyr1Bnk.Text = Banks.PlayerBanks[0].ToString("c");
            lblPlyr2Bnk.Text = Banks.PlayerBanks[1].ToString("c");
            lblPlyr3Bnk.Text = Banks.PlayerBanks[2].ToString("c");
            lblPlyr4Bnk.Text = Banks.PlayerBanks[3].ToString("c");

            Betcount++;
            if (Betcount == 1)
            {
                Flop();
            }
            else if (Betcount == 2)
            {
                Turn();
            }
            else if (Betcount == 3)
            {
                River();
            }
            else if (Betcount == 4)
            {
                btnCall.Enabled = false;
                btnFold.Enabled = false;
                btnRaise.Enabled = false;
                btnCheck.Enabled = false;
                btnShuffle.Enabled = true;
            }
        }

        private void btnRaise_Click(object sender, EventArgs e)
        {
            //Raise click raises the pot by 50. The game further AI logic is need to progress the round from this point.
            btnRaise.Enabled = false;
            var Bets = new CalcBet(PlayerBets);
            Bets.Raise();
            lblPlyr1Bet.Text = Bets.PlayerBets[0].ToString("c");
            lblPlyr2Bet.Text = Bets.PlayerBets[1].ToString("c");
            lblPlyr3Bet.Text = Bets.PlayerBets[2].ToString("c");
            lblPlyr4Bet.Text = Bets.PlayerBets[3].ToString("c");

            var Banks = new CalcBank(PlayerBanks, PlayerBets);
            Banks.NewBankRoll();
            lblPlyr1Bnk.Text = Banks.PlayerBanks[0].ToString("c");
            lblPlyr2Bnk.Text = Banks.PlayerBanks[1].ToString("c");
            lblPlyr3Bnk.Text = Banks.PlayerBanks[2].ToString("c");
            lblPlyr4Bnk.Text = Banks.PlayerBanks[3].ToString("c");
            lblPot.Text = Bets.BetSum().ToString("c");

            Betcount++;
            if (Betcount == 1)
            {
                Flop();
            }
            else if (Betcount == 2)
            {
                Turn();
            }
            else if (Betcount == 3)
            {
                River();
            }
            else if (Betcount == 4)
            {
                btnCall.Enabled = false;
                btnFold.Enabled = false;
                btnRaise.Enabled = false;
                btnCheck.Enabled = false;
                btnShuffle.Enabled = true;
            }
        }



        private void btnRules_Click(object sender, EventArgs e)
        {
            //rules are displayed when button is clicked
            System.Diagnostics.Process.Start("https://bicyclecards.com/how-to-play/texas-holdem-poker/");
        }

        private void btnShuffle_Click(object sender, EventArgs e)
        {
            NewRound();
            btnShuffle.Enabled = false;
        }

        private void btnFold_Click(object sender, EventArgs e)
        {
            //Fold click ends the round as the logic for the AI to continue playing is not yet completed.
            Flop();
            Turn();
            River();
            btnCall.Enabled = false;
            btnFold.Enabled = false;
            btnRaise.Enabled = false;
            btnCheck.Enabled = false;
            btnShuffle.Enabled = true;
        }
        private void btnCheck_Click(object sender, EventArgs e)
        {
            //Check click simply moves to the next card(s) reveal until further AI logic is developed.
            Betcount++;
            if (Betcount == 1)
            {
                Flop();
            }
            else if (Betcount == 2)
            {
                Turn();
            }
            else if (Betcount == 3)
            {
                River();
            }
            else if (Betcount == 4)
            {
                btnCall.Enabled = false;
                btnFold.Enabled = false;
                btnRaise.Enabled = false;
                btnCheck.Enabled = false;
                btnShuffle.Enabled = true;
            }
            btnShuffle.Enabled = false;
        }
    }
}
