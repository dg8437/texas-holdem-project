﻿namespace SnapCall.Enums
{
    /// <summary>
    /// The following code comment is a section of code sourced from a git project 
    /// that can be found here https://github.com/platatat/SnapCall
    /// Weve attempted to implement some of what can be found there but it is still 
    /// very much a work in progress. All code from said repository will have this 
    /// warning before hand.
    /// </summary>
    public enum Rank
	{
		Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
	}
}
