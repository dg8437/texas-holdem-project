﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnapCall;


namespace WindowsFormsApp3
{
    /// <summary>
    /// The following code comment is a section of code sourced from a git project 
    /// that can be found here https://github.com/platatat/SnapCall
    /// Weve attempted to implement some of what can be found there but it is still 
    /// very much a work in progress. All code from said repository will have this 
    /// warning before hand.
    /// </summary>
    public class HandStrength : IComparable<HandStrength>
	{
		public HandRanking HandRanking { get; set; }
		public List<int> Kickers { get; set; }

		public int CompareTo(HandStrength other)
		{
			if (this.HandRanking > other.HandRanking) return 1;
			else if (this.HandRanking < other.HandRanking) return -1;

			for (var i = 0; i < this.Kickers.Count; i++)
			{
				if (this.Kickers[i] > other.Kickers[i]) return 1;
				if (this.Kickers[i] < other.Kickers[i]) return -1;
			}

			return 0;
		}
	}
}
